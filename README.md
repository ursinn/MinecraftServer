# MinecraftServer

[![Build Status Travis Main](https://img.shields.io/travis/com/ursinn/MinecraftServer/main?logo=travis&label=build%20main)](https://travis-ci.com/ursinn/MinecraftServer)
[![Build Status Travis Develop](https://img.shields.io/travis/com/ursinn/MinecraftServer/develop?logo=travis&label=build%20develop)](https://travis-ci.com/ursinn/MinecraftServer)
[![Build Status Jenkins Main](https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fci.filli-it.ch%2Fjob%2Fursinn%2Fjob%2FMinecraftServer%2Fjob%2Fmain%2F&label=build%20main&logo=jenkins)](https://ci.filli-it.ch/job/ursinn/job/MinecraftServer)
[![Build Status Jenkins Develop](https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fci.filli-it.ch%2Fjob%2Fursinn%2Fjob%2FMinecraftServer%2Fjob%2Fdevelop%2F&label=build%20develop&logo=jenkins)](https://ci.filli-it.ch/job/ursinn/job/MinecraftServer)

[![License: MIT](https://img.shields.io/github/license/ursinn/MinecraftServer)](https://opensource.org/licenses/MIT)

A basic implementation of the Minecraft protocol

![Server list](http://i.imgur.com/m4X3oFv.png)

![](http://i.imgur.com/c0oh6ij.png)
